#!/bin/bash

# Usage: ./generate_raw_data_bs_const_db.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p repro/data
for itr in $(seq 1 $nitrs); do
    for num in 4 8 16 32 64; do
        ops=$((num * 20))
        now=`date`; echo "$now: Running ${num} searches on DB of size 2^20 ..."
        Docker/run-experiment 20 ${ops} >> repro/data/log_20_${num}_bs.out
    done
done
