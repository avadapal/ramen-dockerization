#!/bin/bash

# Usage: ./generate_raw_data_reads.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

# Check whether we have enough RAM to do the largest experiment
if [ "$RAMEN_MAXGB" -gt 39 ]; then
    large_exps="26"
else
    large_exps=""
fi

cd ..
mkdir -p repro/data
for itr in $(seq 1 $nitrs); do
    for size in 16 18 20 22 24 $large_exps; do
        now=`date`; echo "$now: Running 10 reads on DB of size 2^${size} ..."
        Docker/run-experiment ${size} 10 >> repro/data/log_${size}_10_reads.out
    done
done

