#!/bin/bash

# Usage: ./generate_raw_data_reads_ssh.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p repro/data
for itr in $(seq 1 $nitrs); do
    for no in 1 2 3 4 6 8 10; do
        n=$((no*48))
        now=`date`; echo "$now: Running $n reads on size 2^9 ..."
        Docker/run-experiment-ssh 9 ${n} >> repro/data/log_9_${n}_reads.out
    done
done

