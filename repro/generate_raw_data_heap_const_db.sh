#!/bin/bash

# Usage: ./generate_raw_data_heap_const_db.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p repro/data
for itr in $(seq 1 $nitrs); do
    for num in 4 8 16 32; do
        ops=$((num * 114))
        now=`date`; echo "$now: Running $num Heap Extracts ..."
	Docker/run-experiment 20 ${ops} >> repro/data/log_20_${num}_heap.out
    done
done            
