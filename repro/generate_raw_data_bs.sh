#!/bin/bash

# Usage: ./generate_raw_data_bs.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

# Check whether we have enough RAM to do the largest experiment
if [ "$RAMEN_MAXGB" -gt 39 ]; then
    large_exps="26"
else
    large_exps=""
fi


cd ..
mkdir -p repro/data
for itr in $(seq 1 $nitrs); do
    for size in 16 18 20 22 24 $large_exps; do
        now=`date`; echo "$now: Running 1 search on DB of size 2^${size} ..."
        Docker/run-experiment ${size} ${size} >> repro/data/log_${size}_1_bs.out
    done
done
