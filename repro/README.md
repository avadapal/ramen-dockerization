## Building the docker

cd docker
Build the docker image with ./build-docker
Start the dockers with ./start-docker
This will start three dockers, each running one of the parties.


## Setting the network parameters

./set-networking 30ms 100mbit

## Generating raw data for the binary search experiments

### Generating data for Figure 7 a)
Open the file `generate_raw_data_bs_const_db.sh`
Change the variable `nitrs` to the number of times you would like run each experiment
Generate the raw data for Figure 7a) using `sh generate_raw_data_bs_const_db.sh`

### Generating data for Figures 7 b) and 7 c)
Open the file `generate_raw_data_bs_const_db.sh`
Change the variable `nitrs` to the number of times you would like run each experiment
Generate the raw data for Figure 7b) and 7c) using `sh generate_raw_data_bs.sh`


## Generate the raw data for heap experiments

### Generating data Figures 8b) and 8 c)
Open the file `generate_raw_data_heap.sh`
Change the variable `nitrs` to the number of times you would like run each experiment
Generate the raw data for the heap experiments using `sh generate_raw_data_heap.sh`
