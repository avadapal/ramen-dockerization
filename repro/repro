#!/bin/bash

# Reproduce the Ramen experiments from our paper:

# Sajin Sasy, Adithya Vadapalli, Ian Goldberg. PRAC: Round-Efficient
# 3-Party MPC for Dynamic Data Structures

# cd into the directory containing this script (from the bash faq 028)
if [[ $BASH_SOURCE = */* ]]; then
  cd -- "${BASH_SOURCE%/*}/" || exit
fi

# If the PRAC NUMA commands are set, but Ramen-specific ones are not,
# use them for Ramen.
if [ "$RAMEN_NUMA_P0" == "" -a "$PRAC_NUMA_P0" != "" ]; then
    export RAMEN_NUMA_P0="$PRAC_NUMA_P0"
fi
if [ "$RAMEN_NUMA_P1" == "" -a "$PRAC_NUMA_P1" != "" ]; then
    export RAMEN_NUMA_P1="$PRAC_NUMA_P1"
fi
if [ "$RAMEN_NUMA_P2" == "" -a "$PRAC_NUMA_P2" != "" ]; then
    export RAMEN_NUMA_P2="$PRAC_NUMA_P2"
fi

# If the PRAC memory usage is set, use it for Ramen.
if [ "$PRAC_P0_MAXGB" != "" ]; then
    export RAMEN_MAXGB=$((3*PRAC_P0_MAXGB))
elif [ "$PRAC_P02_MAXGB" != "" ]; then
    export RAMEN_MAXGB=$((2*PRAC_P02_MAXGB))
elif [ "$PRAC_MAXGB" != "" ]; then
    export RAMEN_MAXGB=$PRAC_MAXGB
else
    export RAMEN_MAXGB=16
fi

# Allow running only subsets of the experiment suite.  Valid values are
# "test", "all", "none".  ("none" is useful if you just want to re-parse
# the output of existing logs.)  You can also say "single" followed by
# all the arguments to "run" (below) to run a single experiment; for
# example:
# ./repro single 20 1
if [ "$1" = "" ]; then
    whichexps="test"
else
    whichexps="$1"
fi

# The number of operations per run; the graphs in the paper use 3
if [ "$whichexps" = "single" -o "$2" = "" ]; then
    # If there's an explicit experiment on the command line, don't read
    # the next argument as the number of operations.  $numops will be
    # ignored, anyway, since it will be specified as part of the
    # command.
    numiters=3
else
    numiters="$2"
fi

# Run one experiment
# Arguments:
# $1: depth (the ORAM has 2^depth elements)
# $2: number of operations (e.g., 20)
run() {
    now=`date`
    echo "$now: Running $1 $2 ..."
    logfile="ramen_${1}_${2}.out${LOGSUFFIX}"
    mkdir -p data
    ../Docker/run-experiment $1 $2 >> data/$logfile
}

# Parse the output logs.  We run this in the docker in case you don't
# have perl installed on the host.
# Arguments: a list of logfiles
parse() {
    if [ "$RAMEN_PARSE_HOST" = "1" ]; then
        ./parse_logs $*
    else
        cat $* | docker exec -w /root/ramen/repro -i ramen_p0 ./parse_logs
    fi
}

# A very small kick-the-tires test to ensure everything compiled and
# built properly
if [ "$whichexps" = "test" ]; then
    echo "Running test experiment..."
    run 16 1
    echo
    echo "# Test output"
    echo
    parse data/ramen_16_1.out${LOGSUFFIX}
    echo
    echo "# End test output"
    echo
    exit
fi

# Be able to run a single experiment specified on the command line
if [ "$whichexps" = "single" ]; then
    echo "Running single experiment..."
    shift
    run $*
    exit
fi

now=`date`
echo "$now: Starting experiments"

if [ "$whichexps" = "fig6" -o "$whichexps" = "all" ]; then
    echo "Running Figure 6 experiments..."
    # Figure 6(a)
    ./generate_raw_data_reads_const_db.sh ${numiters}
    # Figures 6(b) and 6(c)
    ./generate_raw_data_reads.sh ${numiters}
fi

if [ "$whichexps" = "fig7" -o "$whichexps" = "all" ]; then
    echo "Running Figure 7 experiments..."
    # Figure 7(a)
    ./generate_raw_data_bs_const_db.sh ${numiters}
    # Figures 7(b) and 7(c)
    ./generate_raw_data_bs.sh ${numiters}
fi

if [ "$whichexps" = "fig8" -o "$whichexps" = "all" ]; then
    echo "Running Figure 8 experiments..."
    # Figure 8(a)
    ./generate_raw_data_heap_const_db.sh ${numiters}
    # Figures 8(b) and 8(c)
    ./generate_raw_data_heap.sh ${numiters}
fi

now=`date`
echo "$now: Experiments complete"

# If you specified a custom log suffix, you're going to be parsing the
# outputs differently.
if [ "$LOGSUFFIX" = "" ]; then

parse data/*.out > data/ramen.dat

echo
echo "# Figure 6(a)"
egrep 'Ramen read 20 (16|32|64|128|256|512|1024|2048) .* s$' data/ramen.dat | sort -k4 -n
echo
echo "# Figure 6(b)"
egrep 'Ramen read ([0-9]+) 10 .* s$' data/ramen.dat | sort -k3 -n
echo
echo "# Figure 6(c)"
egrep 'Ramen read ([0-9]+) 10 .* KiB$' data/ramen.dat | sort -k3 -n
echo
echo "# Figure 7(a)"
egrep 'Ramen read 20 (80|160|320|640|1280) .* s$' data/ramen.dat | sort -k4 -n
echo
echo "# Figure 7(b)"
egrep 'Ramen read ([0-9]+) \1 .* s$' data/ramen.dat | sort -k3 -n
echo
echo "# Figure 7(c)"
egrep 'Ramen read ([0-9]+) \1 .* KiB$' data/ramen.dat | sort -k3 -n
echo
echo "# Figure 8(a)"
egrep 'Ramen read 20 (456|912|1824|3648) .* s$' data/ramen.dat | sort -k4 -n
echo
echo "# Figure 8(b)"
egrep 'Ramen read (16 90|18 102|20 114|22 126|24 138|26 150|28 162|30 174) .* s$' data/ramen.dat | sort -k3 -n
echo
echo "# Figure 8(c)"
egrep 'Ramen read (16 90|18 102|20 114|22 126|24 138|26 150|28 162|30 174) .* KiB$' data/ramen.dat | sort -k3 -n
echo
echo "# End figures"

fi
