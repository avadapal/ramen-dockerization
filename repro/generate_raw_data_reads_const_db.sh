#!/bin/bash

# Usage: ./generate_raw_data_reads_const_db.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p repro/data
for itr in $(seq 1 $nitrs); do
    for n in 16 32 64 128 256 512 1024 2048; do
        now=`date`; echo "$now: Running ${n} reads on DB of size 2^20 ..."
        Docker/run-experiment 20 ${n} >> repro/data/log_20_${n}_reads.out
    done
done

